package playground;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

public class TimeSeriesPanel extends JPanel
{
    public TimeSeriesPanel()
    {
        setLayout(new BorderLayout());
        JToolBar toolBar = new JToolBar();
        JPanel flowPanel = new JPanel();
        flowPanel.setLayout(new FlowLayout());
        JTextField countTextField = new JTextField();
        JButton setupButton = new JButton("Nastavit");
        JButton exportButton = new JButton("Export");
        add(toolBar, BorderLayout.NORTH);
        add(flowPanel, BorderLayout.CENTER);
        toolBar.add(countTextField);
        toolBar.add(setupButton);
        toolBar.add(exportButton);
        List<TextField> fields = new ArrayList<>();
        setupButton.addActionListener(e -> {
            int count = Integer.parseInt(countTextField.getText());
            for (int i=0; i<count; i++)
            {
                TextField f = new TextField();
                flowPanel.add(f);
                fields.add(f);
            }
        });

        exportButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                for (TextField f : fields)
                {
                    System.out.println(f.getText());
                }
            }
        });


    }
}
